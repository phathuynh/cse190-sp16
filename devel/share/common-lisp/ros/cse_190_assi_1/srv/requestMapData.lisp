; Auto-generated. Do not edit!


(cl:in-package cse_190_assi_1-srv)


;//! \htmlinclude requestMapData-request.msg.html

(cl:defclass <requestMapData-request> (roslisp-msg-protocol:ros-message)
  ((data_type
    :reader data_type
    :initarg :data_type
    :type cl:string
    :initform ""))
)

(cl:defclass requestMapData-request (<requestMapData-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <requestMapData-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'requestMapData-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cse_190_assi_1-srv:<requestMapData-request> is deprecated: use cse_190_assi_1-srv:requestMapData-request instead.")))

(cl:ensure-generic-function 'data_type-val :lambda-list '(m))
(cl:defmethod data_type-val ((m <requestMapData-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader cse_190_assi_1-srv:data_type-val is deprecated.  Use cse_190_assi_1-srv:data_type instead.")
  (data_type m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <requestMapData-request>) ostream)
  "Serializes a message object of type '<requestMapData-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'data_type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'data_type))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <requestMapData-request>) istream)
  "Deserializes a message object of type '<requestMapData-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'data_type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'data_type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<requestMapData-request>)))
  "Returns string type for a service object of type '<requestMapData-request>"
  "cse_190_assi_1/requestMapDataRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'requestMapData-request)))
  "Returns string type for a service object of type 'requestMapData-request"
  "cse_190_assi_1/requestMapDataRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<requestMapData-request>)))
  "Returns md5sum for a message object of type '<requestMapData-request>"
  "c4537edacf3fa77d156bd28d727d39f5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'requestMapData-request)))
  "Returns md5sum for a message object of type 'requestMapData-request"
  "c4537edacf3fa77d156bd28d727d39f5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<requestMapData-request>)))
  "Returns full string definition for message of type '<requestMapData-request>"
  (cl:format cl:nil "string data_type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'requestMapData-request)))
  "Returns full string definition for message of type 'requestMapData-request"
  (cl:format cl:nil "string data_type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <requestMapData-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'data_type))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <requestMapData-request>))
  "Converts a ROS message object to a list"
  (cl:list 'requestMapData-request
    (cl:cons ':data_type (data_type msg))
))
;//! \htmlinclude requestMapData-response.msg.html

(cl:defclass <requestMapData-response> (roslisp-msg-protocol:ros-message)
  ((data
    :reader data
    :initarg :data
    :type cl:string
    :initform ""))
)

(cl:defclass requestMapData-response (<requestMapData-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <requestMapData-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'requestMapData-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cse_190_assi_1-srv:<requestMapData-response> is deprecated: use cse_190_assi_1-srv:requestMapData-response instead.")))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <requestMapData-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader cse_190_assi_1-srv:data-val is deprecated.  Use cse_190_assi_1-srv:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <requestMapData-response>) ostream)
  "Serializes a message object of type '<requestMapData-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'data))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <requestMapData-response>) istream)
  "Deserializes a message object of type '<requestMapData-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'data) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'data) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<requestMapData-response>)))
  "Returns string type for a service object of type '<requestMapData-response>"
  "cse_190_assi_1/requestMapDataResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'requestMapData-response)))
  "Returns string type for a service object of type 'requestMapData-response"
  "cse_190_assi_1/requestMapDataResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<requestMapData-response>)))
  "Returns md5sum for a message object of type '<requestMapData-response>"
  "c4537edacf3fa77d156bd28d727d39f5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'requestMapData-response)))
  "Returns md5sum for a message object of type 'requestMapData-response"
  "c4537edacf3fa77d156bd28d727d39f5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<requestMapData-response>)))
  "Returns full string definition for message of type '<requestMapData-response>"
  (cl:format cl:nil "string data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'requestMapData-response)))
  "Returns full string definition for message of type 'requestMapData-response"
  (cl:format cl:nil "string data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <requestMapData-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'data))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <requestMapData-response>))
  "Converts a ROS message object to a list"
  (cl:list 'requestMapData-response
    (cl:cons ':data (data msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'requestMapData)))
  'requestMapData-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'requestMapData)))
  'requestMapData-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'requestMapData)))
  "Returns string type for a service object of type '<requestMapData>"
  "cse_190_assi_1/requestMapData")