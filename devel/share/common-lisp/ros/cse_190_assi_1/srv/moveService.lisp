; Auto-generated. Do not edit!


(cl:in-package cse_190_assi_1-srv)


;//! \htmlinclude moveService-request.msg.html

(cl:defclass <moveService-request> (roslisp-msg-protocol:ros-message)
  ((move
    :reader move
    :initarg :move
    :type (cl:vector cl:integer)
   :initform (cl:make-array 2 :element-type 'cl:integer :initial-element 0)))
)

(cl:defclass moveService-request (<moveService-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <moveService-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'moveService-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cse_190_assi_1-srv:<moveService-request> is deprecated: use cse_190_assi_1-srv:moveService-request instead.")))

(cl:ensure-generic-function 'move-val :lambda-list '(m))
(cl:defmethod move-val ((m <moveService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader cse_190_assi_1-srv:move-val is deprecated.  Use cse_190_assi_1-srv:move instead.")
  (move m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <moveService-request>) ostream)
  "Serializes a message object of type '<moveService-request>"
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    ))
   (cl:slot-value msg 'move))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <moveService-request>) istream)
  "Deserializes a message object of type '<moveService-request>"
  (cl:setf (cl:slot-value msg 'move) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'move)))
    (cl:dotimes (i 2)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<moveService-request>)))
  "Returns string type for a service object of type '<moveService-request>"
  "cse_190_assi_1/moveServiceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'moveService-request)))
  "Returns string type for a service object of type 'moveService-request"
  "cse_190_assi_1/moveServiceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<moveService-request>)))
  "Returns md5sum for a message object of type '<moveService-request>"
  "b0d2d869e19e2f5145af6c7ec18ac7d0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'moveService-request)))
  "Returns md5sum for a message object of type 'moveService-request"
  "b0d2d869e19e2f5145af6c7ec18ac7d0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<moveService-request>)))
  "Returns full string definition for message of type '<moveService-request>"
  (cl:format cl:nil "int32[2] move~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'moveService-request)))
  "Returns full string definition for message of type 'moveService-request"
  (cl:format cl:nil "int32[2] move~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <moveService-request>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'move) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <moveService-request>))
  "Converts a ROS message object to a list"
  (cl:list 'moveService-request
    (cl:cons ':move (move msg))
))
;//! \htmlinclude moveService-response.msg.html

(cl:defclass <moveService-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass moveService-response (<moveService-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <moveService-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'moveService-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cse_190_assi_1-srv:<moveService-response> is deprecated: use cse_190_assi_1-srv:moveService-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <moveService-response>) ostream)
  "Serializes a message object of type '<moveService-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <moveService-response>) istream)
  "Deserializes a message object of type '<moveService-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<moveService-response>)))
  "Returns string type for a service object of type '<moveService-response>"
  "cse_190_assi_1/moveServiceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'moveService-response)))
  "Returns string type for a service object of type 'moveService-response"
  "cse_190_assi_1/moveServiceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<moveService-response>)))
  "Returns md5sum for a message object of type '<moveService-response>"
  "b0d2d869e19e2f5145af6c7ec18ac7d0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'moveService-response)))
  "Returns md5sum for a message object of type 'moveService-response"
  "b0d2d869e19e2f5145af6c7ec18ac7d0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<moveService-response>)))
  "Returns full string definition for message of type '<moveService-response>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'moveService-response)))
  "Returns full string definition for message of type 'moveService-response"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <moveService-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <moveService-response>))
  "Converts a ROS message object to a list"
  (cl:list 'moveService-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'moveService)))
  'moveService-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'moveService)))
  'moveService-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'moveService)))
  "Returns string type for a service object of type '<moveService>"
  "cse_190_assi_1/moveService")