; Auto-generated. Do not edit!


(cl:in-package cse_190_assi_1-srv)


;//! \htmlinclude requestTexture-request.msg.html

(cl:defclass <requestTexture-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass requestTexture-request (<requestTexture-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <requestTexture-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'requestTexture-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cse_190_assi_1-srv:<requestTexture-request> is deprecated: use cse_190_assi_1-srv:requestTexture-request instead.")))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<requestTexture-request>)))
    "Constants for message type '<requestTexture-request>"
  '((:DATA_TYPE . tex))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'requestTexture-request)))
    "Constants for message type 'requestTexture-request"
  '((:DATA_TYPE . tex))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <requestTexture-request>) ostream)
  "Serializes a message object of type '<requestTexture-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <requestTexture-request>) istream)
  "Deserializes a message object of type '<requestTexture-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<requestTexture-request>)))
  "Returns string type for a service object of type '<requestTexture-request>"
  "cse_190_assi_1/requestTextureRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'requestTexture-request)))
  "Returns string type for a service object of type 'requestTexture-request"
  "cse_190_assi_1/requestTextureRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<requestTexture-request>)))
  "Returns md5sum for a message object of type '<requestTexture-request>"
  "4a79fa7af10fb5a7d3cf791cb4b5a34a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'requestTexture-request)))
  "Returns md5sum for a message object of type 'requestTexture-request"
  "4a79fa7af10fb5a7d3cf791cb4b5a34a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<requestTexture-request>)))
  "Returns full string definition for message of type '<requestTexture-request>"
  (cl:format cl:nil "string data_type=tex~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'requestTexture-request)))
  "Returns full string definition for message of type 'requestTexture-request"
  (cl:format cl:nil "string data_type=tex~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <requestTexture-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <requestTexture-request>))
  "Converts a ROS message object to a list"
  (cl:list 'requestTexture-request
))
;//! \htmlinclude requestTexture-response.msg.html

(cl:defclass <requestTexture-response> (roslisp-msg-protocol:ros-message)
  ((data
    :reader data
    :initarg :data
    :type cl:string
    :initform ""))
)

(cl:defclass requestTexture-response (<requestTexture-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <requestTexture-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'requestTexture-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cse_190_assi_1-srv:<requestTexture-response> is deprecated: use cse_190_assi_1-srv:requestTexture-response instead.")))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <requestTexture-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader cse_190_assi_1-srv:data-val is deprecated.  Use cse_190_assi_1-srv:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <requestTexture-response>) ostream)
  "Serializes a message object of type '<requestTexture-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'data))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <requestTexture-response>) istream)
  "Deserializes a message object of type '<requestTexture-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'data) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'data) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<requestTexture-response>)))
  "Returns string type for a service object of type '<requestTexture-response>"
  "cse_190_assi_1/requestTextureResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'requestTexture-response)))
  "Returns string type for a service object of type 'requestTexture-response"
  "cse_190_assi_1/requestTextureResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<requestTexture-response>)))
  "Returns md5sum for a message object of type '<requestTexture-response>"
  "4a79fa7af10fb5a7d3cf791cb4b5a34a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'requestTexture-response)))
  "Returns md5sum for a message object of type 'requestTexture-response"
  "4a79fa7af10fb5a7d3cf791cb4b5a34a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<requestTexture-response>)))
  "Returns full string definition for message of type '<requestTexture-response>"
  (cl:format cl:nil "string data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'requestTexture-response)))
  "Returns full string definition for message of type 'requestTexture-response"
  (cl:format cl:nil "string data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <requestTexture-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'data))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <requestTexture-response>))
  "Converts a ROS message object to a list"
  (cl:list 'requestTexture-response
    (cl:cons ':data (data msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'requestTexture)))
  'requestTexture-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'requestTexture)))
  'requestTexture-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'requestTexture)))
  "Returns string type for a service object of type '<requestTexture>"
  "cse_190_assi_1/requestTexture")