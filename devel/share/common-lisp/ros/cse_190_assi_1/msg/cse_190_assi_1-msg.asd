
(cl:in-package :asdf)

(defsystem "cse_190_assi_1-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "temperatureMessage" :depends-on ("_package_temperatureMessage"))
    (:file "_package_temperatureMessage" :depends-on ("_package"))
    (:file "RobotProbabilities" :depends-on ("_package_RobotProbabilities"))
    (:file "_package_RobotProbabilities" :depends-on ("_package"))
  ))