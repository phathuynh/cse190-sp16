; Auto-generated. Do not edit!


(cl:in-package cse_190_assi_1-msg)


;//! \htmlinclude temperatureMessage.msg.html

(cl:defclass <temperatureMessage> (roslisp-msg-protocol:ros-message)
  ((temperature
    :reader temperature
    :initarg :temperature
    :type cl:float
    :initform 0.0))
)

(cl:defclass temperatureMessage (<temperatureMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <temperatureMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'temperatureMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cse_190_assi_1-msg:<temperatureMessage> is deprecated: use cse_190_assi_1-msg:temperatureMessage instead.")))

(cl:ensure-generic-function 'temperature-val :lambda-list '(m))
(cl:defmethod temperature-val ((m <temperatureMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader cse_190_assi_1-msg:temperature-val is deprecated.  Use cse_190_assi_1-msg:temperature instead.")
  (temperature m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <temperatureMessage>) ostream)
  "Serializes a message object of type '<temperatureMessage>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'temperature))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <temperatureMessage>) istream)
  "Deserializes a message object of type '<temperatureMessage>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'temperature) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<temperatureMessage>)))
  "Returns string type for a message object of type '<temperatureMessage>"
  "cse_190_assi_1/temperatureMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'temperatureMessage)))
  "Returns string type for a message object of type 'temperatureMessage"
  "cse_190_assi_1/temperatureMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<temperatureMessage>)))
  "Returns md5sum for a message object of type '<temperatureMessage>"
  "694ab1a51fd38693b5cadd94c1ae252d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'temperatureMessage)))
  "Returns md5sum for a message object of type 'temperatureMessage"
  "694ab1a51fd38693b5cadd94c1ae252d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<temperatureMessage>)))
  "Returns full string definition for message of type '<temperatureMessage>"
  (cl:format cl:nil "float32 temperature~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'temperatureMessage)))
  "Returns full string definition for message of type 'temperatureMessage"
  (cl:format cl:nil "float32 temperature~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <temperatureMessage>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <temperatureMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'temperatureMessage
    (cl:cons ':temperature (temperature msg))
))
