// Generated by gencpp from file cse_190_assi_1/requestMapData.msg
// DO NOT EDIT!


#ifndef CSE_190_ASSI_1_MESSAGE_REQUESTMAPDATA_H
#define CSE_190_ASSI_1_MESSAGE_REQUESTMAPDATA_H

#include <ros/service_traits.h>


#include <cse_190_assi_1/requestMapDataRequest.h>
#include <cse_190_assi_1/requestMapDataResponse.h>


namespace cse_190_assi_1
{

struct requestMapData
{

typedef requestMapDataRequest Request;
typedef requestMapDataResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct requestMapData
} // namespace cse_190_assi_1


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::cse_190_assi_1::requestMapData > {
  static const char* value()
  {
    return "c4537edacf3fa77d156bd28d727d39f5";
  }

  static const char* value(const ::cse_190_assi_1::requestMapData&) { return value(); }
};

template<>
struct DataType< ::cse_190_assi_1::requestMapData > {
  static const char* value()
  {
    return "cse_190_assi_1/requestMapData";
  }

  static const char* value(const ::cse_190_assi_1::requestMapData&) { return value(); }
};


// service_traits::MD5Sum< ::cse_190_assi_1::requestMapDataRequest> should match 
// service_traits::MD5Sum< ::cse_190_assi_1::requestMapData > 
template<>
struct MD5Sum< ::cse_190_assi_1::requestMapDataRequest>
{
  static const char* value()
  {
    return MD5Sum< ::cse_190_assi_1::requestMapData >::value();
  }
  static const char* value(const ::cse_190_assi_1::requestMapDataRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::cse_190_assi_1::requestMapDataRequest> should match 
// service_traits::DataType< ::cse_190_assi_1::requestMapData > 
template<>
struct DataType< ::cse_190_assi_1::requestMapDataRequest>
{
  static const char* value()
  {
    return DataType< ::cse_190_assi_1::requestMapData >::value();
  }
  static const char* value(const ::cse_190_assi_1::requestMapDataRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::cse_190_assi_1::requestMapDataResponse> should match 
// service_traits::MD5Sum< ::cse_190_assi_1::requestMapData > 
template<>
struct MD5Sum< ::cse_190_assi_1::requestMapDataResponse>
{
  static const char* value()
  {
    return MD5Sum< ::cse_190_assi_1::requestMapData >::value();
  }
  static const char* value(const ::cse_190_assi_1::requestMapDataResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::cse_190_assi_1::requestMapDataResponse> should match 
// service_traits::DataType< ::cse_190_assi_1::requestMapData > 
template<>
struct DataType< ::cse_190_assi_1::requestMapDataResponse>
{
  static const char* value()
  {
    return DataType< ::cse_190_assi_1::requestMapData >::value();
  }
  static const char* value(const ::cse_190_assi_1::requestMapDataResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // CSE_190_ASSI_1_MESSAGE_REQUESTMAPDATA_H
